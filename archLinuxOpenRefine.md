Tilmann Sager's instructions:

1. Install an AUR helper like yay, pacaur or aurman.

2. Type 'yay -Syu openrefine' to download and install the package (see
https://aur.archlinux.org/packages/openrefine/ for further details)

If you want to clone the git repository, see
https://aur.archlinux.org/openrefine.git .

3. If not installed yet a JDK-Version (usually the latest) will be
installed too.

4. To start openrefine, open a terminal and type 'openrefine'. The
browser, e.g. Firefox, should automatically start and open a localhost
page with openrefine.