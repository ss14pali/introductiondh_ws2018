* [Syllabus](https://git.informatik.uni-leipzig.de/koentges/introductiondh_ws2018/blob/master/Syllabus.md)
* [Lecture Slides](https://git.informatik.uni-leipzig.de/koentges/introductiondh_ws2018/blob/master/LectureSlides.md)
* [Seminar Slides](https://git.informatik.uni-leipzig.de/koentges/introductiondh_ws2018/blob/master/SeminarSlides.md)
* [OpenOLAT](https://olat.informatik.uni-leipzig.de/auth/RepositoryEntry/78446592/CourseNode/94445043373967)